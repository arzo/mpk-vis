package pwr.gwt.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import pwr.gwt.client.GreetingService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl  extends RemoteServiceServlet implements GreetingService
{

	public String doPost(String targeturl, String data) {
		URL url;
		HttpURLConnection connection = null;
		try {
			// Create connection
			url = new URL(targeturl);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(data.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		} finally {

			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	String url = "http://pasazer.mpk.wroc.pl/position.php";
	String data = "busList[bus][]=100&busList[bus][]=103&busList[bus][]=105&busList[bus][]=107&busList[bus][]=109&busList[bus][]=110&busList[bus][]=113&busList[bus][]=114&busList[bus][]=115&busList[bus][]=116&busList[bus][]=118&busList[bus][]=119&busList[bus][]=120&busList[bus][]=122&busList[bus][]=125&busList[bus][]=126&busList[bus][]=127&busList[bus][]=128&busList[bus][]=129&busList[bus][]=130&busList[bus][]=131&busList[bus][]=132&busList[bus][]=133&busList[bus][]=134&busList[bus][]=136&busList[bus][]=140&busList[bus][]=141&busList[bus][]=142&busList[bus][]=144&busList[bus][]=145&busList[bus][]=146&busList[bus][]=147&busList[bus][]=149&busList[bus][]=240&busList[bus][]=241&busList[bus][]=243&busList[bus][]=245&busList[bus][]=246&busList[bus][]=247&busList[bus][]=249&busList[bus][]=250&busList[bus][]=251&busList[bus][]=253&busList[bus][]=255&busList[bus][]=257&busList[bus][]=259&busList[bus][]=305&busList[bus][]=310&busList[bus][]=319&busList[bus][]=325&busList[bus][]=331&busList[bus][]=403&busList[bus][]=406&busList[bus][]=409&busList[bus][]=435&busList[bus][]=602&busList[bus][]=607&busList[bus][]=609&busList[bus][]=612&busList[bus][]=701&busList[bus][]=a&busList[bus][]=c&busList[bus][]=d&busList[bus][]=k&busList[bus][]=n&busList[tram][]=2&busList[tram][]=3&busList[tram][]=5&busList[tram][]=6&busList[tram][]=7&busList[tram][]=8&busList[tram][]=9&busList[tram][]=10&busList[tram][]=11&busList[tram][]=14&busList[tram][]=15&busList[tram][]=17&busList[tram][]=20&busList[tram][]=23&busList[tram][]=24&busList[tram][]=71&busList[tram][]=151&busList[tram][]=158&busList[tram][]=717&busList[tram][]=0l&busList[tram][]=0p&busList[tram][]=31+plus&busList[tram][]=32+plus&busList[tram][]=33+plus&busList[tram][]=150l&busList[tram][]=150p&busList[train]";
	public String greetServer(String input) throws IllegalArgumentException {
		return doPost(url, data);
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html
	 *            the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	public static void main(String[] args) {
		GreetingServiceImpl gr = new GreetingServiceImpl();
		gr.greetServer("Artur");
		String ans;
		System.out.println(ans=gr.doPost(gr.url,gr.data));
	    net.sf.json.JSONArray json = (net.sf.json.JSONArray) net.sf.json.JSONSerializer.toJSON(ans);
	    for(int i=0; i<json.size(); ++i) {
	    	System.out.println(i+"="+json.getJSONObject(i));
	    }
	}
}
