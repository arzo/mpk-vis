package pwr.gwt.client;

import java.util.HashMap;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.maps.gwt.client.ArrayHelper;
import com.google.maps.gwt.client.ControlPosition;
import com.google.maps.gwt.client.GoogleMap;
import com.google.maps.gwt.client.LatLng;
import com.google.maps.gwt.client.MapOptions;
import com.google.maps.gwt.client.MapTypeId;
import com.google.maps.gwt.client.Marker;
import com.google.maps.gwt.client.MarkerImage;
import com.google.maps.gwt.client.MarkerOptions;
import com.google.maps.gwt.client.MarkerShape;
import com.google.maps.gwt.client.Point;
import com.google.maps.gwt.client.Size;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Mpk_vis implements EntryPoint {

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		addMap();
	}
	

	  private void addMap() {
		    Element mapDiv = Document.get().getElementById("map_canvas");
		    MapOptions myOptions = MapOptions.create();
		    myOptions.setZoom(initialZoom);
		    myOptions.setCenter(CHICAGO);
		    myOptions.setMapTypeId(MapTypeId.ROADMAP);
		    map = GoogleMap.create(mapDiv, myOptions);

		    final Panel panel = new StatefulButtonPanel();

		    RootPanel.get().add(panel);

		    // Add button as a map control.
		    map.getControls().get(
		        new Double(ControlPosition.TOP_RIGHT.getValue()).intValue()).push(
		        panel.getElement());
		
	}


	  private static final LatLng CHICAGO = LatLng.create(51,17);
	  private static final double initialZoom = 12;

	  private GoogleMap map;
	  HashMap<Integer, Marker> markers = new HashMap();
	  LatLng home = CHICAGO;
	  double zoom = initialZoom;

	  class StatefulButtonPanel extends VerticalPanel {


	    public StatefulButtonPanel() {
	      Button homeBtn = new Button("Home", new ClickHandler() {
	        public void onClick(ClickEvent event) {
	          map.setCenter(home);
	          map.setZoom(zoom);
	        }
	      });

	      Button setBtn = new Button("Set Home", new ClickHandler() {
	        public void onClick(ClickEvent event) {
	          home = map.getCenter();
	          zoom = map.getZoom();
	        }
	      });
	      Button updatePos = new Button("Update positions", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  update();
				}
			});

	      // Add button to the root panel. (register it on the GWT side)
	      add(homeBtn);
	      add(setBtn);
	      add(updatePos);
	    }
	  }
	  
	  void update() {

			AsyncCallback callback = new AsyncCallback() {
				public void onSuccess(Object result) {
					String jsontxt=(String)result;
					JSONArray ar = (JSONArray) JSONParser.parseLenient(jsontxt);
					for (int i = 0; i < ar.size(); i++) {
						
						double x=ar.get(i).isObject().get("x").isNumber().doubleValue();
						double y=ar.get(i).isObject().get("y").isNumber().doubleValue();
						System.out.println(ar.get(i));
						int k=Integer.parseInt(ar.get(i).isObject().get("k").isString().stringValue());
						String name=ar.get(i).isObject().get("name").isString().stringValue();
						String BEACH_FLAG = "http://dummyimage.com/25x25/ffffff/000000&text="+name;
						String SHADOW_ICON = "http://labs.google.com/ridefinder/images/mm_20_red.png";
						if(markers.containsKey(k)) {
							markers.get(k).setPosition(home=LatLng.create(x, y));} 
						else {
							MarkerImage icon = MarkerImage.create(BEACH_FLAG,
									Size.create(48, 48), Point.create(0, 0),
									Point.create(0, 32));
							MarkerImage shadow = MarkerImage.create(SHADOW_ICON,
									Size.create(37, 32), Point.create(0, 0),
									Point.create(0, 32));
							MarkerShape shape = MarkerShape.create();
							shape.setCoords(ArrayHelper.toJsArrayNumber(1, 1, 1,
									20, 18, 20, 18, 1));
							shape.setType("poly");
							MarkerOptions markerOpts = MarkerOptions.create();
							markerOpts.setPosition(home = LatLng.create(x, y));
							markerOpts.setMap(map);
							markerOpts.setIcon(icon);
							markerOpts.setShadow(shadow);
							markerOpts.setShape(shape);
							markerOpts.setTitle(name);
							markerOpts.setZindex(10);

							markers.put(k, Marker.create(markerOpts));
						}
					}
				}

				public void onFailure(Throwable caught) {
					System.err.println(caught);
				}
			};

			greetingService.greetServer("el",callback);
	  }

}
