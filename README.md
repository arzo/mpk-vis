### Some keywords what was used ###

* Eclipse
* AppEngine
* [GWT](https://gwtproject.org)
* JSON parser
* performing POST request in Java
* Google maps API

### Data source ###

* http://pasazer.mpk.wroc.pl/jak-jezdzimy/mapa-pozycji-pojazdow 
* http://dummyimage.com/ 

### How do I get set up? ###

* install Eclipse
* install GWT plugin to eclipise (along with SDK)
* create new project from existing project and meet download all jars: apache-commons.jar commons-beanutils-1.8.0.jar commons-lang-2.3.jar ezmorph-1.0.2.jar gwt-maps.jar json-lib-2.4-jdk15.jar
* deploy 
* on your local machine by clicking run as 'project name'
* or on [appspot.com](appspot.com)

### Demo ###

Demo available [here](http://mpkloco.appspot.com/Mpk_vis.html) (sorry, but this works only on [Firefox 22](https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/22.0b6/linux-x86_64/pl/) and older Chrome and Safari versions - due to some [GWT limitations](https://groups.google.com/forum/#!topic/Google-Web-Toolkit/QSEjbhhHB4g)). 

Please use https://browserling.com/queue?uri=http%3A%2F%2Fmpkloco.appspot.com%2FMpk_vis.html&browser=firefox&version=22.0.